package com.oreillyauto.service;

import com.oreillyauto.domain.Cart;

public interface CartService {

    void addCart(Cart cart);

}
