package com.oreillyauto.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.oreillyauto.domain.Cart;
import com.oreillyauto.service.CartService;

@Controller
public class CartController {

    @Autowired
    CartService cartService;
    
  
    @GetMapping(value="/cart")
    public String getCart(){
        
        return "cart";
    }
    
    // cart/addCart
    
    @PostMapping(value="/cart/addCart")
    public void addCart(@RequestBody Cart cart, HttpSession session) {
        
        cartService.addCart(cart);
    }
    
    
    
}
