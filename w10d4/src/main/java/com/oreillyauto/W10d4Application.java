package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class W10d4Application {

	public static void main(String[] args) {
		SpringApplication.run(W10d4Application.class, args);
	}

}
